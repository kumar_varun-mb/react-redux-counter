import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { decrement, increment } from '../state/action'

export function Counter(props) {
    const counter = useSelector((state) => state[props.name]);
    const dispatch = useDispatch();

    return (
        <div className='counter'>
            <span>{counter ? counter : 0}</span>
            <button onClick={() => dispatch(increment(props.name))}>Increment</button>
            <button onClick={() => dispatch(decrement(props.name))}>Decrement</button>
        </div>
    )
}


