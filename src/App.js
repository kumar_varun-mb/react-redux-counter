import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Counter } from './Components/Counter';
import { useSelector, useDispatch } from 'react-redux'
import { createNewCounter } from './state/action';

function App() {
  const state = useSelector(state => state)
  const dispatch = useDispatch();
  return (
    <div className='App'>
      {
        (Object.keys(state).length != 0)
          ?
          Object.entries(state).map((key) => {
            return (
              <Counter name={key[0]} />
            )
          })
          :
          null
      }
      <button onClick={() => {
        dispatch(createNewCounter(`counter${Object.keys(state).length + 1}`))

      }}>Create Counter</button>
    </div>
  );
}

export default App;