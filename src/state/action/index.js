
export const increment = (name) => {
    return {
        type: 'Increment',
        name
    }
}

export const decrement = (name) => {
    return {
        type: 'Decrement',
        name
    }
}

export const createNewCounter = (name) => {
    return {
        type: 'createNewCounter',
        name
    }
}