
const counterReducer = (state = {}, action) => {
    const updatedState = { ...state }
    switch (action.type) {
        case "Increment":
            if (!updatedState[action.name])
                updatedState[action.name] = 0;
            updatedState[action.name] += 1;
            return updatedState;

        case "Decrement":
            if (!updatedState[action.name])
                updatedState[action.name] = 0;
            updatedState[action.name] -= 1;
            return updatedState;
        case "createNewCounter":
            updatedState[action.name] = 0;
            return updatedState;
        default:
            return state;
    }
}

export default counterReducer;